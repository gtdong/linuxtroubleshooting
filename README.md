# Linuxtroubleshooting

## Mission
My mission is to build a home for those who have trouble in using linux.Here I give some troubleshooting examples when using linux.Welcome to communicate with me!

## Contents

* [programming](#programming)
  * [git](#git)
  * [nodejs](#nodejs)
  * [python](#python)
  
* [system_service](#system_service)
  * [shell](#shell)
  * [webservice](#webservice)
  
* [project](#project)
  * [blockchain](#blockchain)
  * [application](#application)
  
## programming

### git

* [github中的README语法](doc/github中的README.md语法.md)
* [git多账户配置](doc/git多账户配置.md)

### nodejs
* [node多版本管理](doc/node多版本管理.md)

### python
* [python安装](doc/python安装.md)

## system_service

### shell

* [最大文件打开数限制](doc/最大文件打开数限制.md)
* [系统timewait过多](doc/timewait.md)
* [ansible自动化运维](doc/ansible.md)

### webservice
* [https配置](doc/https.md)
* [postfix](doc/postfix.md)
* [postfix加密](doc/postfix加密.md)
* [apache&nginx](doc/apache&nginx跳转.md)
* [证书链不完整](doc/证书链不完整.md)

## Project

### blockchain

* [peatio](doc/deploy-production-server-centos-7.4.md)
* [Byteball](doc/Byteball.md)
* [eos](doc/eos.md)
* [tinychain](doc/tinychain.md)

### application
* [seafile](doc/seafile.md)
* [ssr](doc/ssr.md)



## Contact me
stevengtdong@gmail.com
